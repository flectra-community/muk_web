# Flectra Community / muk_web

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[muk_web_preview_markdown](muk_web_preview_markdown/) | 1.0.2.0.1| Markdown Preview
[muk_web_preview_audio](muk_web_preview_audio/) | 1.0.2.0.1| Audio Preview
[muk_web_preview_lobject](muk_web_preview_lobject/) | 1.0.1.0.1| PGSQL Large Object Preview Dialog
[muk_web_preview_csv](muk_web_preview_csv/) | 1.0.2.0.1| CSV Preview
[muk_web_preview_image](muk_web_preview_image/) | 1.0.2.0.1| Image Preview
[muk_web_preview_vector](muk_web_preview_vector/) | 1.0.2.0.1| Vector Preview
[muk_web_branding](muk_web_branding/) | 1.0.1.0.0| Branding and Debranding
[muk_web_share](muk_web_share/) | 1.0.2.0.1| Share Button
[muk_web_client_notification](muk_web_client_notification/) | 1.0.1.0.1| Web Client Notification
[muk_web_preview_attachment](muk_web_preview_attachment/) | 1.0.2.0.1| Attachment Preview Dialog
[muk_web_utils](muk_web_utils/) | 1.0.2.0.20| Utility Features
[muk_web_preview](muk_web_preview/) | 1.0.2.1.4| File Preview Dialog
[muk_web_preview_text](muk_web_preview_text/) | 1.0.2.0.1| Text Preview
[muk_web_fields_lobject](muk_web_fields_lobject/) | 1.0.1.0.1| PGSQL Large Objects Field Widget
[muk_web_preview_video](muk_web_preview_video/) | 1.0.2.0.1| Video Preview
[muk_web_preview_msoffice](muk_web_preview_msoffice/) | 1.0.2.0.2| MS Office Preview
[muk_web_export](muk_web_export/) | 1.0.1.0.7| File Export Dialog
[muk_web_preview_mail](muk_web_preview_mail/) | 1.0.2.0.3| Mail Preview
[muk_web_client](muk_web_client/) | 1.0.2.0.2| Odoo Web Client Extension
[muk_web_security](muk_web_security/) | 1.0.1.0.1| Security Features
[muk_web_preview_rst](muk_web_preview_rst/) | 1.0.1.0.2| reStructuredText Preview
[muk_web_client_refresh](muk_web_client_refresh/) | 1.0.2.1.4| Web Client Refresh
[muk_web_export_attachment](muk_web_export_attachment/) | 1.0.1.1.2| Export Odoo Attachments
[muk_web_glyphicons](muk_web_glyphicons/) | 1.0.2.0.1| Bootstrap Glyphicons Support


